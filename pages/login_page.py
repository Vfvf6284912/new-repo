from pages.base_page import BasePage
from selenium.webdriver.common.by import By
import time

changed_url = 'http://automationpractice.com/index.php?controller=authentication&back=my-account'

class LogPageLocators:
    LOGIN_BUTTON = (By.ID, 'SubmitLogin')

class LoginPage(BasePage):

    def should_be_login_page(self):
        self.should_be_login_form()
        self.should_be_register_from()

    def should_be_login_form(self):
        self.driver.find_element_by_id('SubmitCreate')


    def should_be_register_from(self):
        self.is_element_present(*LogPageLocators.LOGIN_BUTTON)

    def should_be_login_url(self):
        get_url = self.driver.current_url
        if get_url == changed_url:
            print(get_url)

class LoginPageReg(BasePage):

    def enter_address(self, address):
        search_add = self.driver.find_element_by_xpath('//*[@id="email"]')
        search_add.click()
        search_add.send_keys(address)
        return search_add

    def enter_pass(self, passwd):
        search_pass = self.driver.find_element_by_xpath('//*[@id="passwd"]')
        search_pass.click()
        search_pass.send_keys(passwd)
        return search_pass

    def click_on_the_sign_in(self):
        butt_can_be_click = self.is_element_present(*LogPageLocators.LOGIN_BUTTON)
        butt_can_be_click.click()
        time.sleep(5)
        answer = self.driver.find_element_by_xpath('//*[@id="center_column"]/div[1]/p')
        return answer


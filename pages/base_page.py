from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class BasePage:

    def __init__(self, driver, url):
        self.driver = driver
        self.driver.implicitly_wait(5)
        self.url = url

    def open(self, url):
        self.driver.get(url)

    def wait_element_clickable(self, find_by, locator, timeout=10):
        element = WebDriverWait(self.driver, timeout).until(EC.element_to_be_clickable((find_by, locator)))
        return element

    def is_element_present(self, locate_by, selector):
        find_element = self.driver.find_element(locate_by, selector)
        return find_element

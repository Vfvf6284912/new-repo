from pages.base_page import BasePage
from pages.login_page import LoginPage
from pages.cart_page import CartPage
from locators.main_page_locators import MainPageLocators
from selenium.webdriver.common.by import By

class MainPageFindLocators:
    ELE = (By.ID, 'contact-link')
    EL = (By.XPATH, '//*[@id="header"]/div[3]/div/div/div[3]/div/a')

class MainPage(BasePage):

    def open_login_page(self):
        login_link = self.wait_element_clickable(*MainPageLocators.LOGIN_LINK)
        login_link.click()
        return LoginPage(self.driver, self.url)

    def open_cart_page(self):
        cart_page = self.is_element_present(*MainPageFindLocators.EL)
        cart_page.click()
        return CartPage(self.driver, self.url)

    def open_contact_page(self):
        cart_page = self.is_element_present(*MainPageFindLocators.ELE)
        cart_page.click()
        return

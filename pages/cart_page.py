from pages.base_page import BasePage

CART_LINK = 'http://automationpractice.com/index.php?controller=order'

class CartPage(BasePage):
    def should_be_cart_page_not_empty(self):
        self.should_be_cart_form()
        self.should_be_process_checkout()
        self.should_be_cart_url()

    def should_be_empty_cart_page(self):
        self.driver.find_element_by_class_name('alert-warning')

    def should_be_cart_form(self):
        self.driver.find_element_by_id('cart_quantity_up_1_1_0_0')

    def should_be_process_checkout(self):
        self.driver.find_element_by_class_name('standard-checkout')

    def should_be_cart_url(self):
        get_url = self.driver.current_url
        if get_url == CART_LINK:
            print(get_url)

class CartPageSumMin(BasePage):

    def total_price_change_if_plus(self):
        plus = self.driver.find_element_by_xpath('//*[@id="cart_quantity_up_1_1_0_0"]/span')
        price = self.driver.find_element_by_id('product_price_1_1_0')
        total_price = self.driver.find_element_by_id('total_product_price_1_1_0')
        plus.click()
        new_price = price * 2
        if new_price == total_price:
            return 'ok'
        elif new_price != total_price:
            print("ERROR on total price")

    def total_price_change_if_minus(self):
        minus = self.driver.find_element_by_xpath('//*[@id="cart_quantity_down_1_1_0_0"]/span')
        price = self.driver.find_element_by_id('product_price_1_1_0')
        total_price = self.driver.find_element_by_id('total_product_price_1_1_0')
        minus.click()
        new_price = total_price - price
        if new_price == total_price:
            return 'ok'
        elif new_price != total_price:
            print("ERROR on total price")

    def if_button_delete_on_page(self):
        button_delete = self.driver.find_element_by_id('icon-trash')
        button_delete.click()







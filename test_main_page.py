from pages.main_page import MainPage
import time

LINK = "http://automationpractice.com/index.php"

def test_user_can_login_page(browser):

    main_page = MainPage(browser, LINK)
    main_page.open(LINK)
    time.sleep(20)
    login_page = main_page.open_login_page()
    login_page.should_be_login_page()


def test_user_can_cart_page(browser):

    main_page = MainPage(browser, LINK)
    main_page.open(LINK)
    time.sleep(5)
    cart_page = main_page.open_cart_page()
    cart_page.should_be_empty_cart_page()


def test_user_can_add_to_cart(browser):

    main_page = MainPage(browser, LINK)
    main_page.open(LINK)
    dress_to_cart = browser.find_element_by_xpath('//*[@id="homefeatured"]/li[1]/div/div[2]/div[2]/a[1]/span')
    dress_to_cart.click()


def test_user_can_contact_page(browser):

    main_page = MainPage(browser, LINK)
    main_page.open(LINK)
    time.sleep(5)
    cont_page = main_page.open_contact_page()
    print(cont_page)
